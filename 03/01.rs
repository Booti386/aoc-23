use std::fs::File;
use std::io::{BufReader, BufRead};
use std::time::Instant;

use aoc_2023::prelude::*;

fn is_symbol(c: u8) -> bool {
	c != b'.' && !c.is_ascii_digit()
}

fn line_has_symbol_at_index(line: &Option<String>, i: usize) -> bool {
	line
		.as_ref()
		.is_some_and(|l| l
			.as_bytes()
			.get(i)
			.is_some_and(|c|
				is_symbol(*c)))
}

fn main()
{
	let file = File::open("03/input.txt").unwrap();

	let ts_begin = Instant::now();

	let reader = BufReader::new(file);
	let mut lines = reader
		.lines()
		.fuse();

	let mut prev = Option::<String>::None;
	let mut cur = lines.next_ascii();
	let mut next = lines.next_ascii();

	let mut total = 0;

	while let Some(ref line) = cur {
		let lb = line.as_bytes();
		let mut idx_end = 0;

		loop {
			let mut idx_start = None;

			for i in idx_end..lb.len() {
				let c = lb[i];
				if c.is_ascii_digit() {
					idx_start = Some(i);
					break;
				}
			}

			if idx_start.is_none() {
				break;
			}

			let mut got_symbol = false;

			let idx_start = idx_start.unwrap();

			if !got_symbol && idx_start > 0 {
				let i = idx_start - 1;

				got_symbol =
					line_has_symbol_at_index(&prev, i) ||
					line_has_symbol_at_index(&cur, i) ||
					line_has_symbol_at_index(&next, i);
			}

			idx_end = lb.len();
			for i in idx_start..lb.len() {
				let c = lb[i];

				if !got_symbol {
					got_symbol =
						line_has_symbol_at_index(&prev, i) ||
						line_has_symbol_at_index(&next, i);
				}

				if !c.is_ascii_digit() {
					idx_end = i;
					break;
				}
			}

			if !got_symbol {
				let i = idx_end;

				got_symbol =
					line_has_symbol_at_index(&prev, i) ||
					line_has_symbol_at_index(&cur, i) ||
					line_has_symbol_at_index(&next, i);
			}

			if !got_symbol {
				continue;
			}

			let num = line[idx_start..idx_end].parse::<u32>().unwrap();
			total += num;
		}

		prev = cur;
		cur = next;
		next = lines.next_ascii();
	}

	let ts_end = Instant::now();

	println!("{}", total);

	let dur = ts_end.duration_since(ts_begin);
	println!("Duration: {}s", dur.as_secs_f64());
}
