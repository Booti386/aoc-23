use std::fs::File;
use std::io::{BufReader, BufRead};
use std::ops::Range;
use std::time::Instant;

use aoc_2023::prelude::*;

fn parse_seeds(lines: &mut impl Iterator<Item = String>) -> Vec<Range<u64>> {
	let locs = lines
		.next()
		.unwrap()
		.strip_prefix("seeds:")
		.unwrap()
		.split_whitespace()
		.map(|loc| loc
			.parse::<u64>()
			.unwrap())
		.map(|loc| loc..loc+1)
		.collect();

	assert!(lines
		.next()
		.unwrap()
		.trim()
		.is_empty());

	locs
}

#[derive(Debug)]
struct CatMappings {
	src: String,
	dst: String,
	map: Vec<(Range<u64>, Range<u64>)>,
}

fn ranges_remap(locs: &mut Vec<Range<u64>>, mappings: impl Iterator<Item = (Range<u64>, Range<u64>)>) {
	let mut remapped_locs = Vec::new();

	for mapping in mappings {
		let mut i = 0;
		while i < locs.len() {
			let loc = &locs[i];
			let (loc_start, loc_end) = (loc.start, loc.end);

			if loc_end > mapping.0.start && loc_start < mapping.0.end {
				if loc_start < mapping.0.start {
					locs.push(loc_start..mapping.0.start);
				}
				if loc_end > mapping.0.end {
					locs.push(mapping.0.end..loc_end);
				}

				let start = mapping.1.start + loc_start.max(mapping.0.start) - mapping.0.start;
				let end = mapping.1.start + loc_end.min(mapping.0.end) - mapping.0.start;
				remapped_locs.push(start..end);

				locs.swap_remove(i);

				// The next element is now at i, so do not increase i
				continue;
			}

			i += 1;
		}
	}

	locs.append(&mut remapped_locs);
}

fn parse_cat_mappings(lines: &mut impl Iterator<Item = String>) -> CatMappings {
	let (src, dst) = lines
		.next()
		.unwrap()
		.trim_end()
		.strip_suffix("map:")
		.unwrap()
		.trim()
		.split_once("-to-")
		.map(|(src, dst)|
			(src.to_owned(), dst.to_owned()))
		.unwrap();

	let map = lines
		.map_while(|line| line
			.trim()
			.is_not_empty()
			.then_some(line))
		.map(|l| {
			let mut it = l
				.split_whitespace();

			let (dst_start, src_start, len) = (
				it.next().unwrap().parse().unwrap(),
				it.next().unwrap().parse().unwrap(),
				it.next().unwrap().parse::<u64>().unwrap(),
			);

			assert!(it.next().is_none());

			(src_start..src_start+len, dst_start..dst_start+len)
		})
		.collect();

	CatMappings {
		src,
		dst,
		map,
	}
}

fn main()
{
	let file = File::open("05/input.txt").unwrap();

	let ts_begin = Instant::now();

	let reader = BufReader::new(file);
	let mut lines = reader
		.lines()
		.map(|l| l.unwrap())
		.peekable();

	let mut locs = parse_seeds(&mut lines);

	let mut next_cat = "seed".to_owned();

	while lines.peek().is_some() {
		let cat_mappings = parse_cat_mappings(&mut lines);

		assert!(cat_mappings.src == next_cat);
		next_cat = cat_mappings.dst;

		ranges_remap(&mut locs, cat_mappings.map.into_iter());
	}

	assert!(next_cat == "location");

	let res = locs
		.iter()
		.map(|loc| loc.start)
		.min()
		.unwrap();

	let ts_end = Instant::now();

	println!("{}", res);

	let dur = ts_end.duration_since(ts_begin);
	println!("Duration: {}s", dur.as_secs_f64());
}
