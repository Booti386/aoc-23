use std::fs::File;
use std::io::{BufReader, BufRead};
use std::time::Instant;

use itertools::{Itertools, FoldWhile};

fn name_to_offset(name: &str) -> u16 {
	assert!(name.len() == 3);
	assert!(name.find(|c: char| !c.is_ascii_uppercase()).is_none());

	name
		.as_bytes()
		.into_iter()
		.fold(0, |acc, c|
			(acc * 26) + (*c - b'A' as u8) as u16)
}

fn is_start_node(ip: usize) -> bool {
	// Ends with 'A'
	ip % 26 == 0
}

fn is_end_node(ip: usize) -> bool {
	// Ends with 'Z'
	ip % 26 == 25
}

fn main()
{
	let file = File::open("08/input.txt").unwrap();

	let ts_begin = Instant::now();

	let reader = BufReader::new(file);
	let mut lines = reader
		.lines()
		.map(|l| l.unwrap());

	let line = lines
		.next()
		.unwrap();
	let instrs = line
		.chars()
		.enumerate()
		.cycle();

	lines
		.next()
		.unwrap()
		.is_empty()
		.then_some(())
		.expect("Missing empty line");

	let mut mem: [Option<(u16, u16)>; 26*26*26] = [None; 26*26*26];
	let mut ips_start = Vec::new();

	lines
		.for_each(|line| {
			let (name, contents) = line
				.split_once('=')
				.unwrap();
	
			let (left, right) = contents
				.trim()
				.strip_prefix("(")
				.unwrap()
				.strip_suffix(")")
				.unwrap()
				.split_once(',')
				.unwrap();

			let offset = name_to_offset(name.trim()) as usize;
			let left_offset = name_to_offset(left.trim());
			let right_offset = name_to_offset(right.trim());

			if is_start_node(offset) {
				ips_start.push(offset);
			}

			mem[offset] = Some((left_offset, right_offset));
		});

	let mem = mem;

	let res = ips_start
		.iter()
		.map(|ip| {
			let mut ip = *ip;
			let mut loop_point = None;

			let step_end = instrs
				.clone()
				.fold_while(0u64, |acc, (idx, instr)| {
					if is_end_node(ip) {
						if let Some((prev_idx, prev_ip, _)) = loop_point {
							if idx == prev_idx && ip == prev_ip {
								return FoldWhile::Done(acc);
							}
						}

						loop_point = Some((idx, ip, acc));
					}
		
					let cur = mem[ip]
						.expect("Node without value");
		
					ip = match instr {
						'L' => cur.0 as usize,
						'R' => cur.1 as usize,
						_ => panic!("Unknown instr: '{}'", instr),
					};
		
					FoldWhile::Continue(acc + 1)
				})
				.into_inner();

			let before_loop_length = loop_point.unwrap().2;
			let loop_length = step_end - before_loop_length;

			// Make sure the problem is trivial...
			assert!(before_loop_length == loop_length);

			loop_length
		})
		.reduce(|acc, loop_length|
			num::integer::lcm(acc, loop_length))
		.unwrap();

	let ts_end = Instant::now();

	println!("{}", res);

	let dur = ts_end.duration_since(ts_begin);
	println!("Duration: {}s", dur.as_secs_f64());
}
