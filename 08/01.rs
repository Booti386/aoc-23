use std::fs::File;
use std::io::{BufReader, BufRead};
use std::time::Instant;

use itertools::{Itertools, FoldWhile};

fn name_to_offset(name: &str) -> u16 {
	assert!(name.len() == 3);
	assert!(name.find(|c: char| !c.is_ascii_uppercase()).is_none());

	name
		.as_bytes()
		.into_iter()
		.fold(0, |acc, c|
			(acc * 26) + (*c - b'A' as u8) as u16)
}

fn main()
{
	let file = File::open("08/input.txt").unwrap();

	let ts_begin = Instant::now();

	let reader = BufReader::new(file);
	let mut lines = reader
		.lines()
		.map(|l| l.unwrap());

	let line = lines
		.next()
		.unwrap();
	let mut instrs = line
		.chars()
		.cycle();

	lines
		.next()
		.unwrap()
		.is_empty()
		.then_some(())
		.expect("Missing empty line");

	let mut mem: [Option<(u16, u16)>; 26*26*26] = [None; 26*26*26];

	lines
		.for_each(|line| {
			let (name, contents) = line
				.split_once('=')
				.unwrap();
	
			let (left, right) = contents
				.trim()
				.strip_prefix("(")
				.unwrap()
				.strip_suffix(")")
				.unwrap()
				.split_once(',')
				.unwrap();

			let offset = name_to_offset(name.trim()) as usize;
			let left_offset = name_to_offset(left.trim());
			let right_offset = name_to_offset(right.trim());

			mem[offset] = Some((left_offset, right_offset));
		});

	let mem = mem;

	let ip_start = name_to_offset("AAA") as usize;
	let ip_end = name_to_offset("ZZZ") as usize;

	let mut ip = ip_start;

	let res = instrs
		.fold_while(0u64, |acc, instr| {
			if ip == ip_end {
				return FoldWhile::Done(acc);
			}

			let cur = mem[ip]
				.expect("Node without value");

			ip = match instr {
				'L' => cur.0 as usize,
				'R' => cur.1 as usize,
				_ => panic!("Unknown instr: '{}'", instr),
			};

			FoldWhile::Continue(acc + 1)
		})
		.into_inner();

	let ts_end = Instant::now();

	println!("{}", res);

	let dur = ts_end.duration_since(ts_begin);
	println!("Duration: {}s", dur.as_secs_f64());
}
