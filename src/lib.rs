use std::fmt::Display;
use std::io::Result as IOResult;
use std::ops::Deref;

pub mod prelude {
	pub use super::ExpectAscii;
	pub use super::Flatten;
	pub use super::IsNotEmpty;
	pub use super::IteratorNextAscii;
}

pub trait ExpectAscii {
	fn expect_ascii(self) -> Self;
}

impl<T: Deref<Target = str> + Display> ExpectAscii for T {
	fn expect_ascii(self) -> Self {
		if !self.is_ascii() {
			panic!("Non-ASCII characters in line: {}", self);
		}
		self
	}
}

pub trait Flatten<T> {
	fn flatten(self) -> Option<T>;
}

impl<T, E> Flatten<T> for Option<Result<T, E>> {
	fn flatten(self) -> Option<T> {
		self.map(|e| e.ok()).flatten()
	}
}

pub trait IsNotEmpty {
	fn is_not_empty(self) -> bool;
}

impl<T: Deref<Target = str> + Display> IsNotEmpty for T {
	fn is_not_empty(self) -> bool {
		!self.is_empty()
	}
}

pub trait IteratorNextAscii<T, It: Iterator<Item = IOResult<T>>> {
	fn next_ascii(&mut self) -> Option<T>;
}

impl<T: Deref<Target = str> + Display, It: Iterator<Item = IOResult<T>>> IteratorNextAscii<T, It> for It {
	fn next_ascii(&mut self) -> Option<T> {
		self
			.next()
			.flatten()
			.and_then(|s|
				Some(s.expect_ascii()))
	}
}
