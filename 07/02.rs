use std::fs::File;
use std::io::{BufReader, BufRead};
use std::time::Instant;

use itertools::Itertools;

fn card_label_to_strength(label: char) -> u64 {
	match label {
		'J' => 0x0,
		'2' => 0x1,
		'3' => 0x2,
		'4' => 0x3,
		'5' => 0x4,
		'6' => 0x5,
		'7' => 0x6,
		'8' => 0x7,
		'9' => 0x8,
		'T' => 0x9,
		'Q' => 0xA,
		'K' => 0xB,
		'A' => 0xC,
		_ => panic!("Unknown card label '{}'", label),
	}
}

fn hand_to_type(hand: &str) -> u64 {
	hand
		.chars()
		.filter(|c| *c != 'J')
		.counts()
		.into_values()
		.sorted_unstable()
		.rev()
		.fold(0u64, |acc, cnt|
			(acc << 4) | (cnt as u64))
}

fn hand_to_strength(hand: &str) -> u64 {
	let ty = hand_to_type(hand);
	let strength = match ty {
		0x11111 => 1,
		0x02111 => 2,
		0x01111 => 2,
		0x00221 => 3,
		0x00311 => 4,
		0x00211 => 4,
		0x00111 => 4,
		0x00032 => 5,
		0x00022 => 5,
		0x00041 => 6,
		0x00031 => 6,
		0x00021 => 6,
		0x00011 => 6,
		0x00005 => 7,
		0x00004 => 7,
		0x00003 => 7,
		0x00002 => 7,
		0x00001 => 7,
		0x00000 => 7,
		_ => panic!("Unhandled case: {}", ty),
	};

	hand
		.chars()
		.fold(strength, |acc, c|
			(acc << 4) | card_label_to_strength(c))
}

fn main()
{
	let file = File::open("07/input.txt").unwrap();

	let ts_begin = Instant::now();

	let reader = BufReader::new(file);
	let lines = reader
		.lines()
		.map(|l| l.unwrap());

	let res: u64 = lines
		.map(|line| {
			let mut fields = line
				.split_whitespace();

			let hand = fields
				.next()
				.unwrap();

			let bid: u64 = fields
				.next()
				.unwrap()
				.parse()
				.unwrap();

			let strength = hand_to_strength(hand);

			(strength, bid)
		})
		.sorted_unstable_by_key(|(strength, _)| *strength)
		.enumerate()
		.map(|(rank, (_, bid))|
			(rank as u64 + 1) * bid)
		.sum();

	let ts_end = Instant::now();

	println!("{}", res);

	let dur = ts_end.duration_since(ts_begin);
	println!("Duration: {}s", dur.as_secs_f64());
}
