use std::fs;
use std::time::Instant;

fn main()
{
	let data = fs::read_to_string("06/input.txt").unwrap();

	let ts_begin = Instant::now();

	let mut lines = data
		.lines();

	let times = lines
		.next()
		.unwrap()
		.strip_prefix("Time:")
		.unwrap()
		.split_whitespace()
		.map(|e| e
			.parse::<u64>()
			.unwrap());

	let distances = lines
		.next()
		.unwrap()
		.strip_prefix("Distance:")
		.unwrap()
		.split_whitespace()
		.map(|e| e
			.parse::<u64>()
			.unwrap());

	let entries = times.zip(distances);

	let res: u64 = entries
		.map(|(time, distance)| {
			let delta = time * time - 4 * distance;
			let sqrtdelta = (delta as f64).sqrt();
			let time = time as f64;

			let r0 = ((time - sqrtdelta) / 2.0).trunc() as u64 + 1;
			let r1 = ((time + sqrtdelta) / 2.0).ceil() as u64 - 1;

			r1 - r0 + 1
		})
		.product();

	let ts_end = Instant::now();

	println!("{}", res);

	let dur = ts_end.duration_since(ts_begin);
	println!("Duration: {}s", dur.as_secs_f64());
}
