use std::fs;
use std::time::Instant;

use itertools::Itertools;

fn main()
{
	let data = fs::read_to_string("06/input.txt").unwrap();

	let ts_begin = Instant::now();

	let mut lines = data
		.lines();

	let time = lines
		.next()
		.unwrap()
		.strip_prefix("Time:")
		.unwrap()
		.split_whitespace()
		.collect_vec()
		.join("")
		.parse::<u64>()
		.unwrap();

	let distance = lines
		.next()
		.unwrap()
		.strip_prefix("Distance:")
		.unwrap()
		.split_whitespace()
		.collect_vec()
		.join("")
		.parse::<u64>()
		.unwrap();

	let delta = time * time - 4 * distance;
	let sqrtdelta = (delta as f64).sqrt();
	let time = time as f64;

	let r0 = ((time - sqrtdelta) / 2.0).trunc() as u64 + 1;
	let r1 = ((time + sqrtdelta) / 2.0).ceil() as u64 - 1;

	let res = r1 - r0 + 1;

	let ts_end = Instant::now();

	println!("{}", res);

	let dur = ts_end.duration_since(ts_begin);
	println!("Duration: {}s", dur.as_secs_f64());
}
