use std::fs::File;
use std::io::{BufReader, BufRead};
use std::time::Instant;

fn main()
{
	let file = File::open("02/input.txt").unwrap();

	let ts_begin = Instant::now();

	let reader = BufReader::new(file);
	let lines = reader
		.lines()
		.map(|l| l.unwrap());

	let res: usize = lines
		.map(|line| {
			let line = line.strip_prefix("Game ").unwrap();
			let (id, data) = line.split_once(':').unwrap();
			let _id = id.parse::<usize>().unwrap();

			let set = data
				.split(&[';', ','])
				.fold((0, 0, 0), |mut acc, amount| {
					let mut it = amount
						.trim()
						.split_whitespace();

					let cnt = it.next().unwrap();
					let ty = it.next().unwrap();
					assert!(it.next().is_none());

					let cnt = cnt.parse::<usize>().unwrap();

					match ty {
						"red"   => acc.0 = acc.0.max(cnt),
						"green" => acc.1 = acc.1.max(cnt),
						"blue"  => acc.2 = acc.2.max(cnt),
						_ => panic!("Unknown cube type: {}", ty),
					};

					acc
				});

			set.0 * set.1 * set.2
		})
		.sum();

	let ts_end = Instant::now();

	println!("{}", res);

	let dur = ts_end.duration_since(ts_begin);
	println!("Duration: {}s", dur.as_secs_f64());
}
