use std::fs::File;
use std::io::{BufReader, BufRead};
use std::time::Instant;

fn main()
{
	let file = File::open("02/input.txt").unwrap();

	let ts_begin = Instant::now();

	let reader = BufReader::new(file);
	let lines = reader
		.lines()
		.map(|l| l.unwrap());

	let res: usize = lines
		.filter_map(|line| {
			let line = line.strip_prefix("Game ").unwrap();
			let (id, data) = line.split_once(':').unwrap();
			let id = id.parse::<usize>().unwrap();

			data
				.split(';')
				.all(|draw| draw
					.split(',')
					.try_fold((0, 0, 0), |mut acc, amount| {
						let mut it = amount
							.trim()
							.split_whitespace();

						let cnt = it.next().unwrap();
						let ty = it.next().unwrap();
						assert!(it.next().is_none());

						let cnt = cnt.parse::<usize>().unwrap();

						match ty {
							"red"   => acc.0 += cnt,
							"green" => acc.1 += cnt,
							"blue"  => acc.2 += cnt,
							_ => panic!("Unknown cube type: {}", ty),
						};

						(acc.0 <= 12 && acc.1 <= 13 && acc.2 <= 14).then_some(acc)
					})
					.is_some()
				)
				.then_some(id)
		})
		.sum();

	let ts_end = Instant::now();

	println!("{}", res);

	let dur = ts_end.duration_since(ts_begin);
	println!("Duration: {}s", dur.as_secs_f64());
}
