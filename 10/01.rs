use std::collections::HashMap;
use std::fs::File;
use std::io::{BufReader, BufRead};
use std::time::Instant;

use aoc_2023::ExpectAscii;
use itertools::Itertools;

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum Facing {
	North,
	South,
	East,
	West,
}

impl Facing {
	fn get_compatible(&self) -> Facing {
		match self {
			Self::North => Self::South,
			Self::South => Self::North,
			Self::East => Self::West,
			Self::West => Self::East,
		}
	}
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum Tile {
	Ground,
	Start,
	Horizontal,
	Vertical,
	NorthToEast,
	NorthToWest,
	SouthToEast,
	SouthToWest,
}

impl From<char> for Tile {
	fn from(value: char) -> Self {
		match value {
			'.' => Self::Ground,
			'S' => Self::Start,
			'-' => Self::Horizontal,
			'|' => Self::Vertical,
			'L' => Self::NorthToEast,
			'J' => Self::NorthToWest,
			'F' => Self::SouthToEast,
			'7' => Self::SouthToWest,
			_ => panic!("Cannot unwrap char '{}' to Tile", value),
		}
	}
}

impl Into<char> for Tile {
	fn into(self) -> char {
		match self {
			Self::Ground => '.',
			Self::Start => 'S',
			Self::Horizontal => '-',
			Self::Vertical => '|',
			Self::NorthToEast => 'L',
			Self::NorthToWest => 'J',
			Self::SouthToEast => 'F',
			Self::SouthToWest => '7',
		}
	}
}

impl Tile {
	fn facing(&self) -> Option<(Facing, Facing)> {
		match self {
			Self::Ground => None,
			Self::Start => None,
			Self::Horizontal => Some((Facing::East, Facing::West)),
			Self::Vertical => Some((Facing::North, Facing::South)),
			Self::NorthToEast => Some((Facing::North, Facing::East)),
			Self::NorthToWest => Some((Facing::North, Facing::West)),
			Self::SouthToEast => Some((Facing::South, Facing::East)),
			Self::SouthToWest => Some((Facing::South, Facing::West)),
		}
	}

	fn next_facing(&self, from: Facing) -> Option<Facing> {
		let cur = from.get_compatible();

		match self.facing() {
			None => None,
			Some((a, b)) => {
				if cur == a {
					Some(b)
				} else if cur == b {
					Some(a)
				} else {
					None
				}
			},
		}
	}

	fn next(&self, pos: &(usize, usize), from: Facing) -> Option<(Facing, (usize, usize))> {
		let next = self.next_facing(from);
		match next {
			None => None,
			Some(next) => match next {
				Facing::North => Some((next, (pos.0, pos.1.wrapping_sub(1)))),
				Facing::South => Some((next, (pos.0, pos.1 + 1))),
				Facing::East => Some((next, (pos.0 + 1, pos.1))),
				Facing::West => Some((next, (pos.0.wrapping_sub(1), pos.1))),
			},
		}
	}
}

trait IntoTile {
	fn into_tile(self) -> Tile;
}

impl IntoTile for char {
	fn into_tile(self) -> Tile {
		self.into()
	}
}

fn main()
{
	let file = File::open("10/input.txt").unwrap();

	let ts_begin = Instant::now();

	let reader = BufReader::new(file);
	let lines = reader
		.lines()
		.peekable();

	let mut cur = (0usize, 0usize);
	let mut start = None;

	let mut map = HashMap::new();

	for line in lines {
		let line = line.unwrap();
		for c in line.chars() {
			let tile = c.into_tile();

			if tile == Tile::Start {
				assert!(start.is_none(), "Excess start found at {:?}, the first one was at {:?}", cur, start);
				start = Some(cur);
			}

			map.insert(cur, tile);

			cur.0 += 1;
		}

		cur.1 += 1;
		cur.0 = 0;
	}

	let start = start
		.expect("No start found");

	let pos = (start.0.wrapping_sub(1), start.1);
	let mut next = map
		.get(&pos)
		.and_then(|t| t
			.next(&pos, Facing::West));
	if next.is_none() {
		let pos = (start.0 + 1, start.1);
		next = map
			.get(&pos)
			.and_then(|t| t
				.next(&pos, Facing::East));
	}
	if next.is_none() {
		let pos = (start.0, start.1.wrapping_sub(1));
		next = map
			.get(&pos)
			.and_then(|t| t
				.next(&pos, Facing::North));
	}
	if next.is_none() {
		let pos = (start.0, start.1 + 1);
		next = map
			.get(&pos)
			.and_then(|t| t
				.next(&pos, Facing::South));
	}

	assert!(next.is_some(), "Stuck after start");
	let mut next = next.unwrap();

	let mut path_size = 2;

	while next.1 != start {
		let cur = next;
		let cur_tile = map
			.get(&cur.1)
			.unwrap();

		next = cur_tile
			.next(&cur.1, cur.0)
			.unwrap();

		path_size += 1;
	}

	let res = path_size / 2;

	let ts_end = Instant::now();

	println!("{}", res);

	let dur = ts_end.duration_since(ts_begin);
	println!("Duration: {}s", dur.as_secs_f64());
}
