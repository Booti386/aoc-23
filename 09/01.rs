use std::fs::File;
use std::io::{BufReader, BufRead};
use std::time::Instant;

use itertools::Itertools;

fn main()
{
	let file = File::open("09/input.txt").unwrap();

	let ts_begin = Instant::now();

	let reader = BufReader::new(file);
	let res: i64 = reader
		.lines()
		.map(|line| {
			let line = line.unwrap();
			let mut values = line
				.split_whitespace()
				.map(|v| v
					.parse::<i64>()
					.unwrap())
				.collect_vec();

			let mut cnt = values.len();
			assert!(cnt > 0, "Line without values");

			let mut acc = 0;
			let mut all_zero = false;

			while !all_zero {
				all_zero = true;

				acc += values[cnt-1];

				for i in 0..cnt-1 {
					let a = values[i];
					let b = values[i + 1];
					let r = b - a;

					values[i] = r;

					if r != 0 {
						all_zero = false;
					}
				}


				cnt -= 1;
			}

			acc
		})
		.sum();

	let ts_end = Instant::now();

	println!("{}", res);

	let dur = ts_end.duration_since(ts_begin);
	println!("Duration: {}s", dur.as_secs_f64());
}
