use std::fs::File;
use std::io::{BufReader, BufRead};
use std::time::Instant;

const ZERO: u32 = '0' as u32;

fn main()
{
	let file = File::open("01/input.txt").unwrap();

	let ts_begin = Instant::now();

	let reader = BufReader::new(file);
	let lines = reader
		.lines()
		.map(|l| l.unwrap());

	let res: u32 = lines
		.map(|line| {
			let first = line.chars().find(char::is_ascii_digit);
			if first.is_none() {
				return 0;
			}

			let last = line.chars().rfind(char::is_ascii_digit);

			let first = first.unwrap() as u32 - ZERO;
			let last = last.unwrap() as u32 - ZERO;

			(first * 10 + last) as u32
		})
		.sum();

	let ts_end = Instant::now();

	println!("{}", res);

	let dur = ts_end.duration_since(ts_begin);
	println!("Duration: {}s", dur.as_secs_f64());
}
