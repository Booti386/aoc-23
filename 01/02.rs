use std::fs::File;
use std::io::{BufReader, BufRead};
use std::iter::Rev;
use std::str::Chars;
use std::time::Instant;

fn digit_to_u8(c: char) -> Option<u8> {
	if !c.is_ascii_digit() {
		return None;
	}

	let czero = '0' as u32;

	let c = c as u32;
	Some((c - czero) as u8)
}

fn extract_digit(mut s: Chars) -> Option<u8> {
	let c0 = s.next()?;
	if let Some(n) = digit_to_u8(c0) {
		return Some(n);
	}

	match c0 {
		'z' => (s.next()? == 'e' && s.next()? == 'r' && s.next()? == 'o').then_some(0),
		'o' => (s.next()? == 'n' && s.next()? == 'e').then_some(1),
		't' => match s.next()? {
			'w' => (s.next()? == 'o').then_some(2),
			'h' => (s.next()? == 'r' && s.next()? == 'e' && s.next()? == 'e').then_some(3),
			_ => None,
		},
		'f' => match s.next()? {
			'o' => (s.next()? == 'u' && s.next()? == 'r').then_some(4),
			'i' => (s.next()? == 'v' && s.next()? == 'e').then_some(5),
			_ => None,
		},
		's' => match s.next()? {
			'i' => (s.next()? == 'x').then_some(6),
			'e' => (s.next()? == 'v' && s.next()? == 'e' && s.next()? == 'n').then_some(7),
			_ => None,
		},
		'e' => (s.next()? == 'i' && s.next()? == 'g' && s.next()? == 'h' && s.next()? == 't').then_some(8),
		'n' => (s.next()? == 'i' && s.next()? == 'n' && s.next()? == 'e').then_some(9),
		_ => None,
	}
}

fn extract_digit_rev(mut s: Rev<Chars>) -> Option<u8> {
	let c0 = s.next()?;
	if let Some(n) = digit_to_u8(c0) {
		return Some(n);
	}

	match c0 {
		'e' => match s.next()? {
			'n' => match s.next()? {
				'o' => Some(1),
				'i' => (s.next()? == 'n').then_some(9),
				_ => None,
			},
			'e' => (s.next()? == 'r' && s.next()? == 'h' && s.next()? == 't').then_some(3),
			'v' => (s.next()? == 'i' && s.next()? == 'f').then_some(5),
			_ => None,
		},
		'o' => match s.next()? {
			'r' => (s.next()? == 'e' && s.next()? == 'z').then_some(0),
			'w' => (s.next()? == 't').then_some(2),
			_ => None,
		},
		'r' => (s.next()? == 'u' && s.next()? == 'o' && s.next()? == 'f').then_some(4),
		'x' => (s.next()? == 'i' && s.next()? == 's').then_some(6),
		'n' => (s.next()? == 'e' && s.next()? == 'v' && s.next()? == 'e' && s.next()? == 's').then_some(7),
		't' => (s.next()? == 'h' && s.next()? == 'g' && s.next()? == 'i' && s.next()? == 'e').then_some(8),
		_ => None,
	}
}

fn main()
{
	let file = File::open("01/input.txt").unwrap();

	let ts_begin = Instant::now();

	let reader = BufReader::new(file);
	let lines = reader
		.lines()
		.map(|l| l.unwrap());

	let res: u32 = lines
		.map(|line| {
			let mut l = line.chars();
			let mut first: Option<u8> = None;

			loop {
				if let Some(v) = extract_digit(l.clone()) {
					first = Some(v);
					break;
				}

				if l.next().is_none() {
					break;
				}
			}

			if first.is_none() {
				return 0;
			}

			let mut l = line.chars().rev();
			let mut last: Option<u8> = None;

			loop {
				if let Some(v) = extract_digit_rev(l.clone()) {
					last = Some(v);
					break;
				}

				if l.next().is_none() {
					break;
				}
			}

			let first = first.unwrap();
			let last = last.unwrap();

			(first * 10 + last) as u32
		})
		.sum();

	let ts_end = Instant::now();

	println!("{}", res);

	let dur = ts_end.duration_since(ts_begin);
	println!("Duration: {}s", dur.as_secs_f64());
}
