use std::collections::{HashMap, HashSet};
use std::fs::File;
use std::io::{BufReader, BufRead};
use std::time::Instant;

fn main()
{
	let file = File::open("04/input.txt").unwrap();

	let ts_begin = Instant::now();

	let reader = BufReader::new(file);
	let lines = reader
		.lines()
		.map(|l| l.unwrap());

	let mut cards = HashMap::<usize, usize>::new();

	let res: usize = lines
		.map(|line| {
			let line = line
				.strip_prefix("Card")
				.unwrap()
				.trim_start();

			let (id, data) = line
				.split_once(':')
				.unwrap();

			let id = id
				.parse::<usize>()
				.unwrap();

			let cur_card_cnt = cards.get(&id).unwrap_or(&0) + 1;

			let (numbers, draw) = data
				.split_once('|')
				.unwrap();

			let numbers = numbers
				.split_whitespace()
				.collect::<HashSet<_>>();

			let cnt = draw
				.split_whitespace()
				.filter(|num| numbers.contains(num))
				.count();

			for id in id+1..id+1+cnt {
				cards.insert(id, cards.get(&id).unwrap_or(&0) + cur_card_cnt);
			}

			cur_card_cnt
		})
		.sum();

	let ts_end = Instant::now();

	println!("{}", res);

	let dur = ts_end.duration_since(ts_begin);
	println!("Duration: {}s", dur.as_secs_f64());
}
