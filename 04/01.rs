use std::collections::HashSet;
use std::fs::File;
use std::io::{BufReader, BufRead};
use std::time::Instant;

fn main()
{
	let file = File::open("04/input.txt").unwrap();

	let ts_begin = Instant::now();

	let reader = BufReader::new(file);
	let lines = reader
		.lines()
		.map(|l| l.unwrap());

	let res: usize = lines
		.map(|line| {
			let line = line
				.strip_prefix("Card")
				.unwrap()
				.trim_start();

			let (id, data) = line
				.split_once(':')
				.unwrap();

			id
				.parse::<usize>()
				.unwrap();

			let (numbers, draw) = data
				.split_once('|')
				.unwrap();

			let numbers = numbers
				.split_whitespace()
				.collect::<HashSet<_>>();

			let cnt = draw
				.split_whitespace()
				.filter(|num| numbers.contains(num))
				.count();

			if cnt == 0 {
				return 0;
			}

			1usize << (cnt - 1)
		})
		.sum();

	let ts_end = Instant::now();

	println!("{}", res);

	let dur = ts_end.duration_since(ts_begin);
	println!("Duration: {}s", dur.as_secs_f64());
}
